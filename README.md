# Inter Test

Project developed for Banco Inter's selective process.

## Build


To compile project:

```bash
mvn install
```

To run unit tests:

```bash
mvn test
```


To run the application:

```bash
mvn spring-boot:run
```

## Usage

After running the application, generate your key pair from the end point below and then save it.

```bash
GET: {{baseUrl}}/security/generate-key-pair
```

You are now ready to register a user by entering their public and private key in the request header.
This is the end point for creating a new user:


```bash
POST: {{baseUrl}}/user

Header params: 
private_key:<string>
public_key:<string>
Content-Type:application/json

Payload:
{
    "email": "<string>",
    "name": "<string>"
}
```

Your information will be encrypted with your public key, so to view your information you will need to enter the private key in the request header.
This is the end point for get your information by your user id:

```bash
GET: {{baseUrl}}/user/:id

Header params: 
private_key:<string>
```
This application provides a single digit calculation API that can be associated with your user.
This is the end point:

```bash
GET: {{baseUrl}}/calculation/calculate-unique-digit?n=<string>&k=<integer>&userId=<ref>
```

You can list the history of calculations made to your user at this end point:
```bash
GET: {{baseUrl}}/calculation/find-all-by-user?userId=<ref>
```