package com.geraldoneto.intertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntertestApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntertestApplication.class, args);
	}

}
