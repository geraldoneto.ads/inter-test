package com.geraldoneto.intertest.abstractions;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@MappedSuperclass
public class BaseEntity {

	@Id
	@GeneratedValue
	@Column(name = "id")
    protected Integer id;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "alteration_date")
    protected Date alterationDate = new Date();
    
    @Column(name = "excluded", columnDefinition = "boolean default false")
    protected boolean excluded = false;
}
