package com.geraldoneto.intertest.abstractions;

import javax.validation.Valid;

import com.geraldoneto.intertest.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.security.spec.InvalidKeySpecException;

public abstract class BaseRestSecurityKeyPairController<E extends BaseEntity, D extends BaseDto> {


    protected abstract BaseService<E, D> getService();

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find all")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "Current Page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "Page Size", dataType = "int", paramType = "query")
    })
    @RequestMapping(method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public Page<D> findAll(Pageable pageable) {
        Page<E> result = getService().findAll(pageable);
        return parseToDTO(result);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find one by id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id of your entity", dataType = "integer", paramType = "path")
    })
    @RequestMapping(value="/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public D findOne(
            @RequestHeader(value = "private_key") String privateKey,
            @PathVariable Integer id) {
        E entity = getService().findOne(id);
        if (entity == null) {
            throw new NotFoundException(String.format("Entity not found with id %s", id));
        }
        return parseToDTO(entity, privateKey);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create new")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public D createNew(
            @RequestHeader(value = "private_key") String privateKey,
            @RequestHeader(value = "public_key") String publicKey,
            @RequestBody @Valid D dto) throws InvalidKeySpecException {
        E entity = getService().save(dto, publicKey);
        return parseToDTO(entity, privateKey);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update an existing")
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
    public D update(
            @RequestHeader(value = "private_key") String privateKey,
            @RequestHeader(value = "public_key") String publicKey,
            @RequestBody @Valid D dto) throws InvalidKeySpecException {
        E entity = getService().save(dto, publicKey);
        return parseToDTO(entity, privateKey);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete one")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id of your entity", dataType = "integer", paramType = "path")
    })
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    public String deleteOne(@PathVariable Integer id) {
        getService().getRepository().deleteById(id);
        return "Removed with success.";
    }

    protected abstract Page<D> parseToDTO(Page<E> page);
    protected abstract D parseToDTO(E entity, String privateKey);
}
