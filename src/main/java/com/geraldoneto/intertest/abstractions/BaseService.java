package com.geraldoneto.intertest.abstractions;

import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.geraldoneto.intertest.model.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.google.common.collect.Lists;

public abstract class BaseService<E extends BaseEntity, D extends BaseDto> {

    @Autowired
    protected SecurityService securityService;

    public abstract BaseRepository<E> getRepository();

    public E findOne(Integer id) {
        if (id == null) {
            return null;
        }
        Optional<E> e = getRepository().findById(id);
        return e.orElse(null);
    }

    public List<E> findAll() {
        return Lists.newArrayList(getRepository().findAll());
    }

    public Page<E> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    public E save(E entity) {
        entity.setAlterationDate(new Date());
        return getRepository().save(entity);
    }

    public E save(D dto, String privateKey) throws InvalidKeySpecException {
        return getRepository().save(parseDtoToEntity(dto, privateKey));
    }

    public abstract E parseDtoToEntity(D dto, String key) throws InvalidKeySpecException;

}
