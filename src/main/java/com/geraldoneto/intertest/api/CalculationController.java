package com.geraldoneto.intertest.api;

import com.geraldoneto.intertest.exception.BadRequestException;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.geraldoneto.intertest.model.domain.Calculation;
import com.geraldoneto.intertest.model.dto.CalculationDto;
import com.geraldoneto.intertest.model.service.CalculationService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("calculation")
public class CalculationController {

	@Autowired
	private CalculationService calculationService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find all by user")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "user id", dataType = "integer", paramType = "query")
	})
	@RequestMapping(path = "/find-all-by-user", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public List<CalculationDto> findAllByUser(@RequestParam(value = "userId") Integer userId) {
		List<Calculation> list = calculationService.findAllByUser(userId);
		return list.stream().map(CalculationDto::new).collect(Collectors.toList());
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Calculate unique digit")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "user id", dataType = "integer", paramType = "query")
	})
	@RequestMapping(path = "/calculate-unique-digit", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public Integer calculateUniqueDigit(
			@RequestParam(value = "userId", required = false) Integer userId,
			@RequestParam(value = "n") String n,
			@RequestParam(value = "k") Integer k) {
		if (!NumberUtils.isCreatable(n)) {
			throw new BadRequestException("The param n must be a number");
		}
		return calculationService.singleDigit(n, k, userId);
	}
}
