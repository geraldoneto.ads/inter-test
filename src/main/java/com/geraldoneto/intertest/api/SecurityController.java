package com.geraldoneto.intertest.api;

import com.geraldoneto.intertest.model.dto.KeyPairDto;
import com.geraldoneto.intertest.model.service.SecurityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("security")
public class SecurityController {

	@Autowired
	private SecurityService securityService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Generate key pair to encrypt and decrypt sensitive information")
	@RequestMapping(path = "/generate-key-pair", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public KeyPairDto generateKeyPair() {
		return securityService.generateKeyPair();
	}

}
