package com.geraldoneto.intertest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.geraldoneto.intertest.abstractions.BaseRestSecurityKeyPairController;
import com.geraldoneto.intertest.abstractions.BaseService;
import com.geraldoneto.intertest.model.domain.User;
import com.geraldoneto.intertest.model.dto.UserDto;
import com.geraldoneto.intertest.model.service.UserService;

@RestController
@RequestMapping("user")
public class UserController extends BaseRestSecurityKeyPairController<User, UserDto> {

	@Autowired
	private UserService userService;
	
	@Override
	protected BaseService<User, UserDto> getService() {
		return userService;
	}

	@Override
	protected Page<UserDto> parseToDTO(Page<User> page) {
		return page.map(UserDto::new);
	}

	@Override
	protected UserDto parseToDTO(User entity, String publicKey) {
		return new UserDto(entity, publicKey);
	}
}
