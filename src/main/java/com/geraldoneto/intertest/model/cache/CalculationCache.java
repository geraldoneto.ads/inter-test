package com.geraldoneto.intertest.model.cache;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class CalculationCache {

    private static final int MAX_CACHE_SIZE = 10;
    private static final Map<String, Integer> cache = Collections.synchronizedMap(new LinkedHashMap<String, Integer>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > MAX_CACHE_SIZE;
        }
    });

    public static Map<String, Integer> getCache() {
        return cache;
    }
}
