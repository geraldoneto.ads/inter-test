package com.geraldoneto.intertest.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.geraldoneto.intertest.abstractions.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "t_calculation")
public class Calculation extends BaseEntity {

    @Column(name = "n")
    private String n;

    @Column(name = "k")
    private Integer k;
    
    @Column(name = "result_digit")
    private Integer resultDigit;
    
    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;

    public Calculation(Integer id) {
        this.id = id;
    }

}