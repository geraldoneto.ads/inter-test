package com.geraldoneto.intertest.model.dto;

import com.geraldoneto.intertest.abstractions.BaseDto;
import com.geraldoneto.intertest.model.domain.Calculation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class CalculationDto extends BaseDto {

	private String n;
	private Integer k;
	private Integer resultDigit;
	private UserDto userDTO;
	
	public CalculationDto(Calculation calc) {
		this.id = calc.getId();
		this.n = calc.getN();
		this.k = calc.getK();
		this.resultDigit = calc.getResultDigit();
		if (calc.getUser() != null) {
			this.userDTO = new UserDto(calc.getUser());
		}
	}
}
