package com.geraldoneto.intertest.model.dto;

import com.geraldoneto.intertest.abstractions.BaseDto;
import com.geraldoneto.intertest.model.domain.User;

import com.geraldoneto.intertest.model.service.SecurityService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.security.spec.InvalidKeySpecException;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends BaseDto {

	private String name;
	private String email;

	public UserDto(User user) {
		this.id = user.getId();
		this.name = "*****";
		this.email = "*****";
	}

	public UserDto(User user, String privateKey) {
		this.id = user.getId();
		try {
			this.name = privateKey == null ? user.getName() : SecurityService.decrypt(user.getName(), privateKey);
			this.email = privateKey == null ? user.getEmail() : SecurityService.decrypt(user.getEmail(), privateKey);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}
}
