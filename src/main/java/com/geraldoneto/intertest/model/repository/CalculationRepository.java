package com.geraldoneto.intertest.model.repository;

import com.geraldoneto.intertest.model.domain.User;
import org.springframework.stereotype.Repository;

import com.geraldoneto.intertest.abstractions.BaseRepository;
import com.geraldoneto.intertest.model.domain.Calculation;

import java.util.List;

@Repository
public interface CalculationRepository extends BaseRepository<Calculation> {

    List<Calculation> findAllByUser(User user);
}
