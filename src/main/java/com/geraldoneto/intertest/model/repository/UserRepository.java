package com.geraldoneto.intertest.model.repository;

import org.springframework.stereotype.Repository;

import com.geraldoneto.intertest.abstractions.BaseRepository;
import com.geraldoneto.intertest.model.domain.User;

@Repository
public interface UserRepository extends BaseRepository<User> {

}
