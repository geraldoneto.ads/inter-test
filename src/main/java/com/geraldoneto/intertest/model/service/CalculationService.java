package com.geraldoneto.intertest.model.service;

import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import com.geraldoneto.intertest.model.cache.CalculationCache;
import com.geraldoneto.intertest.model.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.geraldoneto.intertest.abstractions.BaseRepository;
import com.geraldoneto.intertest.abstractions.BaseService;
import com.geraldoneto.intertest.model.domain.Calculation;
import com.geraldoneto.intertest.model.dto.CalculationDto;
import com.geraldoneto.intertest.model.repository.CalculationRepository;

@Service
public class CalculationService {

	@Autowired
	private CalculationRepository calcRepository;
	@Autowired
	private UserService userService;
	
	public int singleDigit(String n, Integer k, Integer userId) {
		Integer digit = CalculationCache.getCache().computeIfAbsent(n+":"+k, result -> {
			StringBuilder p = new StringBuilder(n);
			IntStream.range(1, k).forEach(i -> p.append(n));
			return calculateDigit(p.toString());
		});
		Calculation calculation = new Calculation();
		calculation.setN(n);
		calculation.setK(k);
		calculation.setUser(userService.findOne(userId));
		calculation.setResultDigit(digit);
		calcRepository.save(calculation);
		return digit;
	}

	private static int calculateDigit(String p) {
		try {
			int sum = Arrays.stream(p.split("")).mapToInt(Integer::parseInt).sum();
			return sum > 9 ? calculateDigit(String.valueOf(sum)) : sum;
		} catch (Exception e) {
			//TODO: A função digitoUnico deverá obrigatoriamente retornar um inteiro.
			System.err.println(e.getMessage());
			return -1;
		}
	}

	public List<Calculation> findAllByUser(Integer userId) {
		User user = userService.findOne(userId);
		return calcRepository.findAllByUser(user);
	}
}
