package com.geraldoneto.intertest.model.service;

import com.geraldoneto.intertest.model.dto.KeyPairDto;
import com.geraldoneto.intertest.security.Encrypt;
import org.springframework.stereotype.Service;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

@Service
public class SecurityService {

    public KeyPairDto generateKeyPair() {
        KeyPair keyPair = Encrypt.generateKeyPair();
        String privateKey = null;
        String publicKey = null;
        try {
            privateKey = Encrypt.savePrivateKey(Objects.requireNonNull(keyPair).getPrivate());
            publicKey = Encrypt.savePublicKey(Objects.requireNonNull(keyPair).getPublic());
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return new KeyPairDto(privateKey, publicKey);
    }

    public static String encrypt(String input, String publicKey) throws InvalidKeySpecException {
        return Encrypt.encrypt(input, Encrypt.loadPublicKey(publicKey));
    }

    public static String decrypt(String input, String privateKey) throws InvalidKeySpecException {
        return Encrypt.decrypt(input, Encrypt.loadPrivateKey(privateKey));
    }
}
