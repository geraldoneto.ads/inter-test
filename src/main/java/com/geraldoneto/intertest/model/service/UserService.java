package com.geraldoneto.intertest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.geraldoneto.intertest.abstractions.BaseRepository;
import com.geraldoneto.intertest.abstractions.BaseService;
import com.geraldoneto.intertest.model.domain.User;
import com.geraldoneto.intertest.model.dto.UserDto;
import com.geraldoneto.intertest.model.repository.UserRepository;

import java.security.spec.InvalidKeySpecException;

@Service
public class UserService extends BaseService<User, UserDto> {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public BaseRepository<User> getRepository() {
		return userRepository;
	}

	@Override
	public User parseDtoToEntity(UserDto dto, String privateKey) throws InvalidKeySpecException {
		User user = new User();
		user.setId(dto.getId());
		user.setName(securityService.encrypt(dto.getName(), privateKey));
		user.setEmail(securityService.encrypt(dto.getEmail(), privateKey));
		return user;
	}
}
