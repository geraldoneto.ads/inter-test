package com.geraldoneto.intertest;

import com.geraldoneto.intertest.model.dto.KeyPairDto;
import com.geraldoneto.intertest.model.dto.UserDto;
import com.geraldoneto.intertest.model.repository.CalculationRepository;
import com.geraldoneto.intertest.model.repository.UserRepository;
import com.geraldoneto.intertest.model.service.CalculationService;
import com.geraldoneto.intertest.model.service.SecurityService;
import com.geraldoneto.intertest.model.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.spec.InvalidKeySpecException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class IntertestApplicationTests {

	@InjectMocks
	private CalculationService calculationService;
	@InjectMocks
	private SecurityService securityService;
	@Mock
	private UserService userService;
	@Mock
	private CalculationRepository calculationRepository;
	@Mock
	private UserRepository userRepository;

	@Test
	public void testUniqueDigit() {
		assertThat(calculationService.singleDigit("524637487", 4, null)).isEqualTo(4);
		assertThat(calculationService.singleDigit("52463x7487", 4, null)).isEqualTo(-1);
		calculationService.findAllByUser(1);
	}

	@Test
	public void testCrypt() throws InvalidKeySpecException {
		String input = "Me contrate!";
		KeyPairDto keys = securityService.generateKeyPair();
		String encryptedInput = SecurityService.encrypt(input, keys.getPublicKey());
		String decryptedInput = SecurityService.decrypt(encryptedInput, keys.getPrivateKey());
		assertThat(decryptedInput).isEqualTo(input);
	}

	@Test
	public void testUserCrud() throws InvalidKeySpecException {
		KeyPairDto keys = securityService.generateKeyPair();
		UserDto dto = new UserDto();
		dto.setEmail("geraldoneto.ads@gmail.com");
		dto.setName("Geraldo Neto");
		userService.save(dto, keys.getPrivateKey());
		userService.findOne(1);
		userService.findAll();
	}

}
